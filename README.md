## INTRODUCTION

This module provides an upgrade path for ckeditor templates created using the
https://www.drupal.org/project/ckeditor_templates_ui module into the CKEditor 5
ready version of https://www.drupal.org/project/ckeditor_templates.

This module does the following two things upon installation:

1. Migrates existing templates created via ckeditor_templates_ui module into the
new entity type available in ckeditor_templates ckeditor5 version.
2. Updates all existing content to wrap existing template embeds with wrapping
markup that gets added around the template when embedding a template in
ckeditor5, namely

```html
<section class="ckeditor-template-wrapper"><div class="ckeditor-template-content"></div></section>
```

The migration of the templates happens right upon installation, whereas the
wrapping of existing template embeds happens during cron runs after the
installation.

## REQUIREMENTS

This module requires the ckeditor_templates module and existing templates
created via ckeditor_templates_ui module. For the upgrade path of existing
templates cron needs to run on the server.

## INSTALLATION

Install this Drupal module as you would normally install a contributed
Drupal module. For more information, visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8

## WARNING

This module will change existing template embeds in your formatted text fields.
This action may break existing layouts and you will want to test this thoroughly
before deploying to production. Create a DB backup before deploying and
installing this module!

## Legacy templates in JS files

If you had this module installed for CKEditor 4, there is no automated upgrade
path for templates that were added in JS files yet. You will have to manually
copy the code in the JS files and paste it into template entities on the UI.

## Events dispatched during migrations

The module provides 3 events that allow customizations of migrated templates and
the templates that get changed in existing content. The 3rd event allows to
skip wrapping of certain templates with the wrappers mentioned above if they are
embedded/nested inside another template as that may break layouts in certain
cases.

Here a quick overview of the events:

### Amending migrated templates

It's also possible to amend templates migrated this way using an event
subscriber that's listening to `CkeditorTemplateMigrationEvent`, for example:

```php
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      CkeditorTemplateMigrationEvent::class => ['onCkeditorTemplateMigration'],
    ];
  }
```

See also `CkeditorTemplatesUiMigrator::migrateCkeditorTemplatesUiTemplates()`.

### Controlling which existing template embeds are wrapped

Subscribe to `AlterTemplateNeedlesEvent` in order to remove certain templates
from the complete list of altered templates by unsetting them from the list.

### Skip wrapping of nested templates

Subscribe to `AlterIncludeNestedEvent` in order to avoid wrapping of certain
existing template embeds if they are nested inside other templates.




