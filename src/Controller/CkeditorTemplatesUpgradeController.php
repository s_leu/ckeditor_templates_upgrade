<?php

namespace Drupal\ckeditor_templates_upgrade\Controller;

use DOMWrap\Document;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Ckeditor Template Upgrade routes.
 */
class CkeditorTemplatesUpgradeController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $html = '<div class="row share-container">
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://chi2023.acm.org/">CHI 2023</a>
        </h3>
        <div>
            <p class="link-calendar">
                April 23 - 28&nbsp;· Hamburg, Germany
            </p>
            <p>
                <a class="link-email" href="adir://groups/8379993">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C047NH3T6M7">#event-chi-2023</a>
            </p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://iclr.cc/">ICLR 2023</a>
        </h3>
        <div>
            <p>
                <span class="link-calendar">May 1 - 5 · Kigali, Rwanda</span>
            </p>
            <p>
                <a class="link-email" href="adir://groups/8379999">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C047CBUUMV2">#event-iclr-2023</a>
            </p>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
</div>
<div class="col-xs-12 col-sm-6 share-block">
    <h3>
        <a href="https://2023.ieeeicassp.org/">ICASSP 2023</a>
    </h3>
    <div>
        <p>
            <span class="link-calendar">June 4 - 9 · </span>Rhodes Island, Greece
        </p>
        <p>
            <a class="link-email" href="adir://groups/8380001">Directory Group</a>
        </p>
        <p>
            <a class="link-slack" href="https://a1350286.slack.com/archives/C0479TR5YMQ">#event-icassp-2023</a>
        </p>
    </div>
    <div>
        &nbsp;
    </div>
</div>
<div class="col-xs-12 col-sm-6 share-block">
    <h3>
        <a href="https://cvpr.thecvf.com/">CVPR 2023</a>
    </h3>
    <div>
        <p>
            <span class="link-calendar">June 18 - 22 · </span>Vancouver, Canada
        </p>
        <p>
            <a class="link-email" href="adir://groups/8380005">Directory Group</a>
        </p>
        <p>
            <a class="link-slack" href="https://apple.slack.com/archives/C02TTD4B6G1">#event-cvpr-2023</a>
        </p>
    </div>
</div>
<p>
    &nbsp;
</p>
<p>
    &nbsp;
</p>
<p>
    &nbsp;
</p>
<p>
    <br>
    <br>
    <br>
    <br>
    dfaasdf
</p>
<div class="row share-container">
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://2023.aclweb.org/">ACL 2023</a>
        </h3>
        <div>
            <span class="link-calendar">July 9 - 14 · </span>Toronto, Canada
        </div>
        <div>
            &nbsp;
        </div>
        <div>
            <a class="link-email" href="adir://groups/8380004">Directory Group</a>
        </div>
        <div>
            <br>
            <a class="link-slack" href="https://a1350286.slack.com/archives/C046VBMS4ET">#event-acl-2023</a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://sigir.org/sigir2023/">SIGIR 2023</a>
        </h3>
        <div>
            <span class="link-calendar">July 23 - 27 · Taipei</span>, Taiwan
        </div>
        <div>
            <br>
            <a class="link-email" href="adir://groups/8380006">Directory Group</a>
        </div>
        <div>
            <br>
            <a class="link-slack" href="https://a1350286.slack.com/archives/C01GD0JMEHY">#</a><a href="https://apple.slack.com/archives/C02TEQKJ42F">event-</a><a href="https://a1350286.slack.com/archives/C047NH5RY65">sigir-2023</a>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
</div>
<div class="row share-container">
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://icml.cc/Conferences/2023">ICML 2023</a>
        </h3>
        <div>
            <p>
                <span class="link-calendar">July 23 - 29 · </span>Honolulu, Hawaii
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380023">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C04739TLF9U">#event-icml-2023</a>
            </p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://mlweb.apple.com/events/2022/interspeech">Interspeech 202</a><a href="https://www.interspeech2023.org/">3</a>
        </h3>
        <div>
            <p>
                <span class="link-calendar">August 20 - 24 · </span>Dublin, Ireland
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380025">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C01GK58GBPW">#</a><a href="https://apple.slack.com/archives/C02TEQLE5A7">event-interspeech-202</a><a href="https://a1350286.slack.com/archives/C047NH6A8LR">3</a>
            </p>
        </div>
        <div>
            <p>
                &nbsp;
            </p>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
</div>
<div class="row share-container">
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://kdd.org/kdd2023/">KDD 2023</a>
        </h3>
        <div>
            <p>
                <span class="link-calendar">August 6 - 10· </span>Long Beach, California
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380029">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C01FY84JURM">#</a><a href="https://apple.slack.com/archives/C02T83HT762">event-kdd-202</a><a href="https://a1350286.slack.com/archives/C04739UEQBG">3</a>
            </p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            <a href="https://iccv2023.thecvf.com/">ICCV 2023</a>
        </h3>
        <div>
            <p>
                <span class="link-calendar">October 2 - October 6 · </span>Paris, France
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380028">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C01FY84JURM">#</a><a href="https://a1350286.slack.com/archives/C047ZK3N7CY">event-iccv-2023</a>
            </p>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
</div>
<div class="row share-container">
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            EMNLP 2023
        </h3>
        <div>
            <p>
                <span class="link-calendar">TBD · </span>TBD
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380033">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://a1350286.slack.com/archives/C01GD0FUVS6">#</a><a href="https://apple.slack.com/archives/C02T05EB1TR">event-emnlp-202</a><a href="https://a1350286.slack.com/archives/C04739URSRL">3</a>
            </p>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 share-block">
        <h3>
            NeurIPS 2023
        </h3>
        <div>
            <p>
                <span class="link-calendar">TBD · TBD</span>
            </p>
            <p>
                <a class="link-email" href="adir://groups/8380031">Directory Group</a>
            </p>
            <p>
                <a class="link-slack" href="https://apple.slack.com/archives/C01G68BSLKY">#</a><a href="https://apple.slack.com/archives/C02TEQLKWUT">event-neurips-202</a><a href="https://a1350286.slack.com/archives/C04739URSRL">3</a>
            </p>
            <p>
                &nbsp;
            </p>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
</div>
<p>
    &nbsp;
</p>
<section class="ckeditor-template-wrapper">
    <div class="ckeditor-template-content">
        <div class="row share-container">
            <div class="col-xs-12 col-sm-6 share-block">
                <h3>
                    EMNLP 2023
                </h3>
                <p>
                    <span class="link-calendar">TBD · </span>TBD
                </p>
                <p>
                    <a class="link-email" href="adir://groups/8380033">Directory Group</a>
                </p>
                <p>
                    <a class="link-slack" href="https://a1350286.slack.com/archives/C01GD0FUVS6">#</a><a href="https://apple.slack.com/archives/C02T05EB1TR">event-emnlp-202</a><a href="https://a1350286.slack.com/archives/C04739URSRL">3</a>
                </p>
            </div>
            <div class="col-xs-12 col-sm-6 share-block">
                <h3>
                    Title
                </h3>
                <div>
                    <a class="icon-date" href="#">Monday, January 1</a>
                    <br>
                    <a class="icon-clock" href="#">6:00 AM - 4:30 PM PST</a>
                    <br>
                    <a class="icon-person" href="#">Person</a>
                </div>
            </div>
            <p>
                &nbsp;
            </p>
        </div>
    </div>
</section>
<p>
    &nbsp;
</p>';

    $doc = new Document();
    $doc->setLibxmlOptions(\LIBXML_HTML_NOIMPLIED | \LIBXML_HTML_NODEFDTD);
    $doc->html($html);
//    $nodes = $doc->find('div.row.share-container');
//    // Returns '3'
//    var_dump($nodes->count());

    // Append as a child node to each <li>
//    $nodes->wrap('<section class="ckeditor-template-wrapper"><div class="ckeditor-template-content"></div></section>');

    //    // Returns '3'
    //    var_dump($nodes->count());

    // Append as a child node to each <li>
    $nodes = $doc->find('.ckeditor-template-wrapper .ckeditor-template-content')->children();
    $nodes->unwrap();
    $nodes = $doc->find('.ckeditor-template-wrapper')->children();
    $nodes->unwrap();

//    // Returns: <html><body><ul><li>First<b>!</b></li><li>Second<b>!</b></li><li>Third<b>!</b></li></ul></body></html>
//    var_dump();

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $nodes->count() . '<code>' . htmlentities($doc->document()->saveHTML()) . '</code>',
    ];

    return $build;
  }

}
