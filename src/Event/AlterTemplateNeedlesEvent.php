<?php

namespace Drupal\ckeditor_templates_upgrade\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event dispatched during collection of template tag needles.
 */
class AlterTemplateNeedlesEvent extends Event {

  protected array $tags;

  public function __construct(array $tags) {
    $this->tags = $tags;
  }

  public function getTemplateNeedles() : array {
    return $this->tags;
  }

  public function setTemplateNeedles(array $tags) : void {
    $this->tags = $tags;
  }

}
