<?php

namespace Drupal\ckeditor_templates_upgrade\Event;

use Drupal\ckeditor_templates\Entity\CKEditorTemplates;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event dispatched during migration of ckeditor_templates_ui templates.
 */
class CkeditorTemplateMigrationEvent extends Event {

  protected CKEditorTemplates $template;

  /**
   * CkeditorTemplateMigrationEvent constructor.
   *
   * @param \Drupal\ckeditor_templates\Entity\CKEditorTemplates $template
   *   The migrated template, with values from the legacy template already set.
   */
  public function __construct(CKEditorTemplates $template) {
    $this->template = $template;
  }

  /**
   * Gets the template entity.
   *
   * @return \Drupal\ckeditor_templates\Entity\CKEditorTemplates
   */
  public function getTemplate() : CKEditorTemplates {
    return $this->template;
  }

  /**
   * Sets the template entity.
   */
  public function setTemplate(CKEditorTemplates $template) : void {
    $this->template = $template;
  }

}
