<?php

namespace Drupal\ckeditor_templates_upgrade\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that allows skipping of nested templates during wrapping process.
 */
class AlterIncludeNestedEvent extends Event {

  protected array $skipped_templates;

  public function __construct(array $tags) {
    $this->skipped_templates = $tags;
  }

  public function getIncludeNestedTemplates() : array {
    return $this->skipped_templates;
  }

  public function setIncludeNestedTemplates(array $skipped) : void {
    $this->skipped_templates = $skipped;
  }

}
