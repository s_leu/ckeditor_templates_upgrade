<?php

namespace Drupal\ckeditor_templates_upgrade\Plugin\Filter;

use DOMWrap\Document;
use Drupal\ckeditor5\Plugin\CKEditor5PluginManagerInterface;
use Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent;
use Drupal\ckeditor_templates_upgrade\UpgradeUtility;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a filter to apply resizing of media images.
 *
 * @Filter(
 *   id = "filter_wrap_existing_templates",
 *   title = @Translation("Wrap existing template embeds"),
 *   description = @Translation("Uses a <code>data-media-width</code> attribute on <code>&lt;drupal-media&gt;</code> tags to apply resizing of media images. This filter needs to run before the <strong>Embed media</strong> filter and requires the <strong>Limit allowed HTML tags and correct faulty HTML</strong> to be active."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_HTML_RESTRICTOR,
 *   weight = 90,
 * )
 */
class FilterWrapExistingTemplates extends FilterBase implements ContainerFactoryPluginInterface {

  protected EntityTypeManagerInterface $entityTypeManager;
  protected CurrentRouteMatch $routeMatch;
  protected UpgradeUtility $upgradeUtility;
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->upgradeUtility = $container->get('ckeditor_templates_upgrade.upgrade_utility');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $route_defaults = $this->routeMatch->getCurrentRouteMatch()->getRouteObject()->getDefaults();
    $processing_in_editor = !empty($route_defaults['_entity_form']);

    if (!$processing_in_editor || empty($text)) {
      return $result;
    }

    return new FilterProcessResult($this->wrapTemplateEmbeds($text));
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p>Applies the resizing on embedded media images inside ckeditor5 generated content when rendering the ckeditor content in Drupal.</p>
        <p>This works via a the <code>data-media-width</code> attribute on <code><drupal-media></code> tags, for example: <code><drupal-media data-media-width="50%"</code>.</p>
      ');
    }
    else {
      return $this->t('You can resize media images by adding the <code>data-media-width</code> attribute on <code><drupal-media></code> tags, for example: <code><drupal-media data-media-width="50%"</code>.');
    }
  }

  protected function wrapTemplateEmbeds(string $value) : string {
    $doc = new Document();
    $doc->setLibxmlOptions(\LIBXML_HTML_NOIMPLIED | \LIBXML_HTML_NODEFDTD);
    $doc->html($value);
    $selectors = \Drupal::service('ckeditor_templates_upgrade.upgrade_utility')
      ->getTemplateEmbedSelectors();

    foreach ($selectors as $template_id => $selector) {
      $nodes = $doc->find($selector);
      if (!$nodes->count()) {
        continue;
      }

      $parents = $nodes->parents('.ckeditor-template-wrapper');
      if ($parents->count()) {
        $include_list = drupal_static('ckeditor_templates_upgrade_skiplist');
        if (is_null($include_list)) {
          /** @var \Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent $event */
          $event = $this->eventDispatcher->dispatch(new AlterIncludeNestedEvent(array_keys($selectors)));
          $include_list = $event->getIncludeNestedTemplates();
        }
        // If a template was removed from the list of templates that are enabled
        // for wrapping in case they are nested, skip it.
        if (!in_array($template_id, $include_list)) {
          continue;
        }
      }

      // Append the template markup to a wrapper which will be up-casted by the
      // ckeditor5 version of the ckeditor_templates module as a widget inside
      // the editor.
      $nodes->wrap('<section class="ckeditor-template-wrapper"><div class="ckeditor-template-content"></div></section>');
    }
    return $doc->document()->saveHTML();
  }

}
