<?php

namespace Drupal\ckeditor_templates_upgrade\Plugin\QueueWorker;

use DOMWrap\Document;
use Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent;
use Drupal\ckeditor_templates_upgrade\UpgradeUtility;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Processes text fields to wrap existing template embeds.
 *
 * @QueueWorker(
 *   id = "ckeditor_templates_upgrade",
 *   title = @Translation("Upgrade ckeditor template embeds."),
 *   cron = {"time" = 5}
 * )
 *
 */
class EntityQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * FieldQueueWorker constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, QueueFactory $queueFactory, EntityTypeManagerInterface $entityTypeManager, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queueFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entityTypeId = $data['entity_type'];
    $entityId = $data['id'];
    $replacementPerformed = FALSE;

    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($entityId);

    if (empty($entity)) {
      return;
    }

    $fieldDefinitions = $entity->getFieldDefinitions();

    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      $translation = $entity->getTranslation($langcode);

      foreach ($fieldDefinitions as $fieldDefinition) {
        if (!in_array($fieldDefinition->getType(), UpgradeUtility::PROCESSED_FIELD_TYPES)) {
          continue;
        }

        $fieldItemList = $translation->get($fieldDefinition->getName());
        if ($fieldItemList->isEmpty()) {
          continue;
        }

        for ($i = 0; $i < $fieldItemList->count(); $i++) {
          $item = $fieldItemList->get($i);
          $value = $item->getValue();
          $originalValue = $value['value'];
          $value['value'] = $this->wrapTemplateEmbeds($value['value']);

          if ($value['value'] != $originalValue) {
            $item->setValue($value);
            $fieldItemList->set($i, $item);
            $replacementPerformed = TRUE;
          }
        }
      }
    }

    if ($replacementPerformed) {
      $entity->save();
    }
  }

  /**
   * Wraps existing template embeds with ckeditor templates wrapper elements.
   *
   * @param string $value
   *   Value of a formatted text field like a node body.
   *
   * @return string
   *   Field value with updated template embeds.
   */
  protected function wrapTemplateEmbeds(string $value) : string {
    $doc = new Document();
    $doc->setLibxmlOptions(\LIBXML_HTML_NOIMPLIED | \LIBXML_HTML_NODEFDTD);
    $doc->html($value);

    // @todo Use static var
    $selectors = drupal_static('ckeditor_template_upgrade_selectors');
    if (!$selectors) {
      $selectors = \Drupal::service('ckeditor_templates_upgrade.upgrade_utility')
        ->getTemplateEmbedSelectors();
    }

    foreach ($selectors as $template_id => $selector) {
      $nodes = $doc->find($selector);
      if (!$nodes->count()) {
        continue;
      }

      $parents = $nodes->parents('.ckeditor-template-wrapper');
      if ($parents->count()) {
        $include_list = drupal_static('ckeditor_templates_upgrade_skiplist');
        if (is_null($include_list)) {
          /** @var \Drupal\ckeditor_templates_upgrade\Event\AlterIncludeNestedEvent $event */
          $event = $this->eventDispatcher->dispatch(new AlterIncludeNestedEvent(array_keys($selectors)));
          $include_list = $event->getIncludeNestedTemplates();
        }
        // If a template was removed from the list of templates that are enabled
        // for wrapping in case they are nested, skip it.
        if (!in_array($template_id, $include_list)) {
          continue;
        }
      }

      // Append the template markup to a wrapper which will be up-casted by the
      // ckeditor5 version of the ckeditor_templates module as a widget inside
      // the editor.
      $nodes->wrap('<section class="ckeditor-template-wrapper"><div class="ckeditor-template-content"></div></section>');
    }
    return $doc->document()->saveHTML();
  }

}
