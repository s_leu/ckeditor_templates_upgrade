<?php

/**
 * @file
 * Post update functions for the CKEditor Templates module.
 */

/**
 * Migrate ckeditor_templates_ui templates into ckeditor_templates entities.
 */
function ckeditor_templates_upgrade_post_update_migrate_ckeditor_templates_ui(&$sandbox) {
  if (!\Drupal::moduleHandler()->moduleExists('ckeditor_templates_ui')) {
    $sandbox['#finished'] = TRUE;
    return;
  }

  if (!isset($sandbox['templates'])) {
    $legacy_templates = \Drupal::entityTypeManager()->getStorage('ckeditor_template')
      ->loadMultiple();
    $sandbox['templates'] = array_chunk($legacy_templates, 5);
  }

  $batch = array_shift($sandbox['templates']);
  $template_formats = array_keys(filter_formats());
  if (is_array($batch) && $template_formats) {
    \Drupal::service('ckeditor_templates_upgrade.template_migrator')
      ->migrateCkeditorTemplatesUiTemplates($batch, $template_formats);
  }

  $sandbox['#finished'] = count($sandbox['templates']) === 0;
}
